<?php


function isBalanced($string)
{
    $stack = [];
    $brackets = [
        ')' => '(',
        '}' => '{',
        ']' => '['
    ];

 
    for ($i = 0; $i < strlen($string); $i++) {
        $char = $string[$i];

        if (in_array($char, ['(', '{', '['])) {
            array_push($stack, $char);
        } elseif (in_array($char, [')', '}', ']'])) {
            if (empty($stack) || array_pop($stack) != $brackets[$char]) {
                return 'NO';
            }
        }
    }

    return empty($stack) ? 'YES' : 'NO';
}

$input1 = "{ [ ( ) ] }";
$input2 = "{ [ ( ] ) }";
$input3 = "{ ( ( [ ] ) [ ] ) [ ] }";

echo isBalanced($input1) . PHP_EOL; 
echo isBalanced($input2) . PHP_EOL; 
echo isBalanced($input3) . PHP_EOL; 
?>
