test bri

Indra Gunawan Wibisono

# How To Run/Show the answer
Test 1 : php 1.php
Test 2 : php 2.php
Test 3 : php 3.php

 


# Balanced Bracket Checker
This repository contains a PHP function to check if the brackets in a given string are balanced.

## Function Description
The function `isBalanced($string)` takes a string containing brackets and returns `YES` if the brackets are balanced and `NO` otherwise.

### Example
1. Input: `{ [ ( ) ] }`
   Output: `YES`
   
2. Input: `{ [ ( ] ) }`
   Output: `NO`
   
3. Input: `{ ( ( [ ] ) [ ] ) [ ] }`
   Output: `YES`

### Complexity Analysis
The isBalanced function uses a stack to keep track of open brackets. The algorithm checks each character in the string one time, so its time complexity is O(n), where n is the length of the string. The space complexity is O(n) in the worst case, which happens when all characters are open brackets.

### Usage
To use the function, simply call it with a string as input:

```php
echo isBalanced("{ [ ( ) ] }"); // Output: YES
    