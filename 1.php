<?php


function getCharWeight($char)
{
    return ord($char) - ord('a') + 1;
}

function checkQueries($string, $queries)
{
    $weights = [];
    $length = strlen($string);
    $i = 0;

    while ($i < $length) {
        $char = $string[$i];
        $weight = getCharWeight($char);
        $weights[$weight] = true;

        $j = $i + 1;
        while ($j < $length && $string[$j] == $char) {
            $weight += getCharWeight($char);
            $weights[$weight] = true;
            $j++;
        }
        $i = $j;
    }

    // Memeriksa setiap query
    $results = [];
    foreach ($queries as $query) {
        if (isset($weights[$query])) {
            $results[] = 'Yes';
        } else {
            $results[] = 'No';
        }
    }

    return $results;
}

// Contoh penggunaan fungsi
$string = "abbcccd";
$queries = [1, 3, 9, 8];

$results = checkQueries($string, $queries);
print_r($results);


