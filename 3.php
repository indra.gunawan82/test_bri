<?php

function highestPalindrome($str, $k, $left = 0, $right = null) {
    // Init
    if ($right === null) {
        $right = strlen($str) - 1;
    }

    //left bigger ?
    if ($left > $right) {
        return $str;
    }

    // left = right
    if ($str[$left] === $str[$right]) {
        return highestPalindrome($str, $k, $left + 1, $right - 1);
    }

    if ($k > 0) {
        $maxChar = max($str[$left], $str[$right]);

        if ($str[$left] !== $maxChar) {
            $str[$left] = $maxChar;
            $k--;
        }
        if ($str[$right] !== $maxChar) {
            $str[$right] = $maxChar;
            $k--;
        }

        return highestPalindrome($str, $k, $left + 1, $right - 1);
    } else {
        return '-1';
    }
}


$input1 = "3943";
$k1 = 1;
echo highestPalindrome($input1, $k1) . PHP_EOL;  

$input2 = "932239";
$k2 = 2;
echo highestPalindrome($input2, $k2) . PHP_EOL;  

?>
